import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';

export default function BottomNav() {
    return (
        <BottomNavigation>
                  <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
        </BottomNavigation>
    );
}