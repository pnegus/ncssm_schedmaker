import React from 'react';
import HeaderBar from './HeaderBar'
import ClassPicker from './ClassPicker'
import './App.css';
import Login from './Login'

function App() {
  return (
    <div className="App">
      <HeaderBar></HeaderBar>
      <ClassPicker></ClassPicker>
      <Login></Login>
    </div>
  );
}

export default App;
