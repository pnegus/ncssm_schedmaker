import React from 'react';
import { GoogleLogin } from 'react-google-login';

const responseGoogle = (response) => {
  console.log(response);
  console.log("success");
}

const responseFail = (response) => {
  console.log(response);
  console.log("fail");
}

class Login extends React.Component{
  render () {
    return (
      <GoogleLogin
      clientId="989781550834-6h710o1bv7qbe66b70p040m2nm4ovafs.apps.googleusercontent.com"
      buttonText="Login"
      onSuccess={responseGoogle}
      onFailure={responseFail}
      cookiePolicy={'single_host_origin'}
    />
    );
  }
}

export default Login;