This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## About this project

Web App intended to improve QOL for NCSSM students when it comes to adding classes to Google Calendar.

Static version of this project is deployed [here](https://negus.patrickyi.gitlab.io/ncssm_schedmaker).

